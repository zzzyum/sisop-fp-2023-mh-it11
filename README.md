# Kelompok IT11 Sistem Operasi

Kelompok IT11 :
| Nama | NRP |
| -------------------------------------- | ---------- |
| Mohammad Arkananta Radithya Taratugang | 5027221003 |
| Fazrul Ahmad Fadhilah | 5027221025 |
| Samuel Yuma Krismata | 5027221029 |

## Soal
### Sistem Database Sederhana

#### Bagaimana Program Diakses

- Program server berjalan sebagai **daemon**.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command **mysql** di **bash**).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.

#### Struktur Direktori (Gitlab)

```
-database/
--[program_database].c
--[settingan_cron_backup]
-client/
--[program_client].c
-dump/
--[program_dump_client].c
```

#### Struktur Direktori (Database)

```
[folder_server_database]
-[program_database]
-database/
--[nama_database]/  → Directory
---[nama_tabel]     → File
```

#### Struktur Direktori (Server)

```
database/
-program_databaseku
-databases/
--database1/
---table11
---table12
--database2/
---table21
---table22
---table23
```

## Penjelasan

### Autentikasi
Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.

#### Format

./[program_client_database] -u [username] -p [password]

```c
int checkUser(const char *username, const char *password)
{
    FILE *fp = fopen("src/users.txt", "r");

    if (fp == NULL)
    {
        printf("Failed to open users file.\n");
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), fp))
    {
        char user[100], pass[100];
        int matched = sscanf(line, "%99[^,],%99[^,\n]", user, pass);

        if (matched >= 2 && strcmp(user, username) == 0 && strcmp(pass, password) == 0)
        {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}
```

- Fungsi membuka file "src/users.txt" untuk membaca daftar pengguna dan kata sandi.
- Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan dan mengembalikan nilai 0, menandakan kegagalan autentikasi.
- Fungsi membaca file baris per baris menggunakan fgets.
- Untuk setiap baris, fungsi menggunakan sscanf untuk memisahkan username dan password.
- Jika username dan password yang diberikan cocok dengan entri yang ada di file, fungsi mengembalikan nilai 1, menandakan keberhasilan autentikasi.
- Jika tidak ada kecocokan ditemukan setelah membaca seluruh file, atau ada kesalahan dalam membaca file, fungsi mengembalikan nilai 0, menandakan kegagalan autentikasi.

![Screenshot_2023-12-17_231836](/uploads/3d835f773e0211338cd838b1aaaccebf/Screenshot_2023-12-17_231836.png)

#### Untuk membuat user
```c
if (strncmp(command, "CREATE USER", 11) == 0)
    {
        char newuser[100], newpass[100];
        sscanf(command, "CREATE USER %s IDENTIFIED BY %s", newuser, newpass);

        FILE *fp = fopen("src/users.txt", "a");
        if (fp == NULL)
        {
            printf("Failed to open users file.\n");
            return -1;
        }

        fprintf(fp, "%s,%s\n", newuser, newpass);
        fclose(fp);

        printf("User created successfully.\n");

        continue;
    }
```

Bagian kode tersebut berfungsi untuk membuat pengguna baru (user) dalam sistem. Berikut adalah penjelasannya:

1. **Pengecekan Hak Akses Root:**
   ```c
   if (getuid() == 0)
   ```
   Pengecekan ini dilakukan untuk memastikan bahwa program dijalankan dengan hak akses root (superuser).

2. **Pengecekan Perintah "CREATE USER":**
   ```c
   if (strncmp(command, "CREATE USER", 11) == 0)
   ```
   Jika perintah yang dimasukkan oleh pengguna diawali dengan "CREATE USER", maka blok berikutnya akan dieksekusi.

3. **Ekstraksi Informasi Username dan Password Baru:**
   ```c
   char newuser[100], newpass[100];
   sscanf(command, "CREATE USER %s IDENTIFIED BY %s", newuser, newpass);
   ```
   Baris ini digunakan untuk mengekstrak username dan password baru dari perintah yang dimasukkan oleh pengguna.

4. **Pembukaan dan Penulisan ke File "src/users.txt":**
   ```c
   FILE *fp = fopen("src/users.txt", "a");
   if (fp == NULL)
   {
       printf("Gagal membuka file pengguna.\n");
       return -1;
   }

   fprintf(fp, "%s,%s\n", newuser, newpass);
   fclose(fp);
   ```
   Kode ini membuka file "src/users.txt" dalam mode append ("a"), menulis informasi pengguna baru ke dalam file tersebut, dan kemudian menutupnya.

5. **Pesan Konfirmasi:**
   ```c
   printf("Pengguna berhasil dibuat.\n");
   ```
   Jika proses penambahan pengguna baru berhasil, pesan ini akan ditampilkan.

![Screenshot_2023-12-17_231729](/uploads/328482c564de64e271197b6cc68576b3/Screenshot_2023-12-17_231729.png)
![Screenshot_2023-12-17_231737](/uploads/b08f8086da73f3902d6760f0ec0c1ce7/Screenshot_2023-12-17_231737.png)

### Autorisasi
1. USE
``` c
int authorizeUser(const char *username, const char *database)
{
    if (getuid() == 0)
    {
        return 1;
    }

    FILE *fp = fopen("src/permissions.txt", "r");
    if (fp == NULL)
    {
        printf("Failed to open permissions file.\n");
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), fp))
    {
        char db[100], user[100];
        int matched = sscanf(line, "%99[^,],%99[^,\n]", db, user);

        if (matched >= 2 && strcmp(db, database) == 0 && strcmp(user, username) == 0)
        {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}
```
Fungsi `authorizeUser` adalah fungsi yang bertujuan untuk mengautorisasi atau menentukan apakah suatu pengguna (user) memiliki izin akses ke suatu database tertentu. Fungsi ini menerima dua parameter, yaitu username yang merupakan nama pengguna yang akan diautorisasi, dan database yang merupakan nama database yang akan diakses.

- **Cek Privilese Root (Superuser)**: Fungsi dimulai dengan melakukan pengecekan apakah pengguna saat ini memiliki privilege superuser (root) dengan menggunakan fungsi `getuid()`. Jika pengguna memiliki privilege root, maka fungsi langsung mengembalikan nilai 1, menandakan bahwa akses diautorisasi.
- **Membuka File Permissions**: Selanjutnya, fungsi mencoba membuka file "src/permissions.txt" yang berisi informasi izin akses user terhadap database. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan dan mengembalikan nilai 0, menandakan bahwa akses tidak diautorisasi.
- **Pemrosesan Setiap Baris pada File**: Fungsi membaca setiap baris pada file permissions menggunakan `fgets` dan memprosesnya. Baris tersebut berisi informasi database dan user yang diotorisasi.
- **Pembandingan Database dan User**: Fungsi menggunakan `sscanf` untuk memisahkan informasi database dan user dari setiap baris. Jika terdapat kecocokan antara database, user, dan parameter yang diberikan, fungsi langsung mengembalikan nilai 1, menandakan bahwa akses diautorisasi.
- **Penutupan File**: Setelah semua baris pada file diproses, file permissions ditutup dengan menggunakan `fclose`.
- **Pengembalian Nilai Default**:
Jika tidak ada kecocokan ditemukan selama proses, fungsi mengembalikan nilai 0, menandakan bahwa akses tidak diautorisasi.

``` c
// client.c

if (strncmp(command, "USE", 3) == 0)
{
    char database[100];
    sscanf(command, "USE %s", database);

    if (!authorizeUser(username, database))
    {
        printf("You don't have permission to access this database.\n");
        continue;
    }
}
```
Kondisi yang digunakan untuk mengecek apakah suatu perintah (command) yang diterima dimulai dengan string "USE". Jika iya, maka dilakukan beberapa langkah untuk memproses perintah tersebut.

- **Pengecekan Perintah "USE"**: Kondisi `strncmp(command, "USE", 3) == 0` mengecek apakah tiga karakter pertama dari string command sama dengan "USE". Jika ya, maka blok kondisional dijalankan, menandakan bahwa perintah yang diterima adalah perintah "USE".
- **Ekstraksi Nama Database**: Setelah memastikan bahwa perintah adalah "USE", dilakukan ekstraksi nama database dari perintah menggunakan sscanf. Nama database disimpan dalam variabel database.
- **Otorisasi Pengguna**: Selanjutnya, dilakukan pengecekan otorisasi pengguna terhadap database yang ingin diakses. Fungsi `authorizeUser` dipanggil dengan parameter username (nama pengguna) dan database. Jika pengguna tidak diautorisasi, maka pesan kesalahan dicetak dan program melanjutkan iterasi selanjutnya dengan perintah continue.
- **Akses Diotorisasi**: Jika pengguna diautorisasi, program melanjutkan untuk menjalankan operasi yang sesuai dengan perintah "USE" pada database tersebut.

``` c
// database.c

if (strncmp(command, "USE", 3) == 0)
{
    char databaseName[50];
    sscanf(command, "USE %s", databaseName);
    strcpy(selectedDB, databaseName);
    
    char response[100];
    snprintf(response, sizeof(response), "User logged to %s", selectedDB);
    send(*client, response, strlen(response), 0);

    continue;
}
```
Kondisi yang digunakan untuk menangani perintah "USE". Blok ini dieksekusi jika tiga karakter pertama dari string command adalah "USE".

- **Pengecekan Perintah "USE"**: Kondisi `strncmp(command, "USE", 3) == 0` memeriksa apakah tiga karakter pertama dari string command sama dengan "USE". Jika kondisi terpenuhi, maka blok kondisional dijalankan, menandakan bahwa perintah yang diterima adalah perintah "USE".
- **Ekstraksi Nama Database**: Menggunakan `sscanf`, blok ini mengekstrak nama database dari perintah "USE" dan menyimpannya dalam variabel databaseName.
- **Penetapan Database Terpilih**: Nama database yang telah diekstrak kemudian disalin ke dalam variabel `selectedDB` menggunakan fungsi strcpy.
- **Pembentukan Pesan Respon**: Selanjutnya, dibentuk pesan respon yang akan dikirimkan ke klien. Pesan ini menyatakan bahwa pengguna telah berhasil masuk ke database yang dipilih. 
- **Mengirim Respon ke Klien**: Pesan respon kemudian dikirimkan ke klien melalui soket (`*client`) dengan menggunakan fungsi send.
- **Melanjutkan Iterasi**: Setelah menjalankan langkah-langkah di atas, program melanjutkan iterasinya ke langkah berikutnya dengan menggunakan command `continue`.

Proses autorisasi berhasil
![USE__success_](/uploads/b346e6c4594ee444f4a73e85e469fd3f/USE__success_.png)

Proses autorisasi gagal
![USE__failed_](/uploads/f707911b319177f8edd60c8f2201706f/USE__failed_.png)

2. GRANT PERMISSION
``` c
void grantPermission(const char *database, const char *username)
{
    if (getuid() != 0)
    {
        printf("Only root can grant permissions.\n");
        return;
    }

    FILE *fp = fopen("src/permissions.txt", "a");
    if (fp == NULL)
    {
        printf("Failed to open permissions file.\n");
        return;
    }

    fprintf(fp, "%s,%s\n", database, username);
    fclose(fp);

    printf("Permission granted successfully.\n");
}
```
Fungsi `grantPermission` adalah suatu fungsi yang digunakan untuk memberikan izin akses pengguna ke suatu database.

- **Pengecekan Privilege Root (Superuser)**: Pertama, fungsi melakukan pengecekan apakah pengguna saat ini memiliki privilege superuser (root) dengan menggunakan kondisi `getuid() != 0`. Jika pengguna bukan merupakan superuser, maka fungsi mencetak pesan kesalahan bahwa hanya root yang dapat memberikan izin, dan fungsi langsung keluar.
- **Membuka File Permissions**: Fungsi mencoba membuka file "src/permissions.txt" dengan mode "a" (append) untuk menambahkan izin baru. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan dan keluar dari fungsi.
- **Penambahan Izin ke File**: Jika file berhasil dibuka, fungsi menggunakan fprintf untuk menulis informasi izin baru ke dalam file. Informasi tersebut berupa pasangan database dan username yang dipisahkan oleh koma. Setelah penulisan, file ditutup dengan menggunakan fclose.
- **Pesan Sukses**: Jika proses penambahan izin berhasil, fungsi mencetak pesan bahwa izin telah diberikan dengan sukses.

``` c
else if (strncmp(command, "GRANT PERMISSION", 16) == 0)
{
    char db[100], grantUser[100];
    sscanf(command, "GRANT PERMISSION %s INTO %s", db, grantUser);
    grantPermission(db, grantUser);

    continue;
}
```
Kondisi yang digunakan untuk menangani perintah yang dimulai dengan "GRANT PERMISSION" dengan memanggil fungsi grantPermission untuk memberikan izin akses ke suatu database kepada pengguna tertentu.

- **Pengecekan Perintah "GRANT PERMISSION"**: Kondisi `strncmp(command, "GRANT PERMISSION", 16) == 0` memeriksa apakah enam belas karakter pertama dari string command sesuai dengan "GRANT PERMISSION". Jika kondisi terpenuhi, blok kondisional dijalankan, menandakan bahwa perintah yang diterima adalah perintah "GRANT PERMISSION".
- **Ekstraksi Nama Database dan Pengguna yang Diberi Izin**: Menggunakan sscanf, blok ini mengekstrak nama database dan pengguna yang akan diberikan izin dari perintah "GRANT PERMISSION". Informasi tersebut disimpan dalam variabel `db` dan `grantUser`.
- **Panggilan Fungsi grantPermission**: Setelah ekstraksi informasi, fungsi grantPermission dipanggil dengan parameter nama database (`db`) dan pengguna yang akan diberikan izin (`grantUser`). Fungsi ini akan melakukan pengecekan privilege root, membuka file izin, menambahkan izin ke file, dan memberikan pesan sukses jika operasi berhasil.
- **Melanjutkan Iterasi**: Setelah menjalankan langkah-langkah di atas, program melanjutkan iterasinya ke langkah berikutnya dengan menggunakan perintah continue.

![GRANT_PERMISSION](/uploads/56ef2a2766300206aabab4b9567ef81e/GRANT_PERMISSION.png)

### DDL
1. CREATE DATABASE
```c
void createDatabase(const char *databaseName, int clientSocket) {
    struct stat st = {0};
    if (stat("databases", &st) == -1) {
        mkdir("databases", 0777);
    }

    char databasePath[100];
    snprintf(databasePath, sizeof(databasePath), "databases/%s", databaseName);

    if (num_databases >= MAX_DATABASES) {
        const char *response = "Maximum databases reached";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (mkdir(databasePath, 0777) == -1) {
        const char *response = "Failed to create database";
        send(clientSocket, response, strlen(response), 0);
        return;
    } else {
        const char *response = "Database created";
        send(clientSocket, response, strlen(response), 0);
    }

    strcpy(databases[num_databases].name, databaseName);
    databases[num_databases].num_tables = 0;
    num_databases++;
}
```
Fungsi `createDatabase` adalah yang bertujuan untuk membuat database baru. Fungsi ini menerima dua parameter: `databaseName`, yang merupakan nama database yang akan dibuat, dan `clientSocket`, yang digunakan untuk mengirim respons balik ke klien.

- **Pengecekan Direktori "databases"**:
Fungsi ini pertama-tama memeriksa apakah direktori "databases" sudah ada atau belum. Jika direktori tersebut tidak ada, maka fungsi akan membuatnya menggunakan fungsi `mkdir` dengan mode izin 0777 (mode ini memberikan izin penuh untuk membaca, menulis, dan menjalankan direktori oleh semua pengguna).
- **Membuat Path Database Baru**:
Fungsi menggunakan `snprintf` untuk membuat path lengkap dari database baru yang akan dibuat dengan format "databases/nama_database".
- **Pengecekan Jumlah Database Maksimum**:
Fungsi juga memeriksa apakah jumlah database yang telah dibuat sudah mencapai batas maksimum `MAX_DATABASES`. Jika sudah, maka fungsi akan mengirim pesan balasan ke klien bahwa jumlah database sudah mencapai batas maksimum yang ditentukan.
- **Membuat Database Baru**:
Jika batas jumlah database belum tercapai, fungsi akan mencoba membuat direktori baru dengan menggunakan `mkdir` pada path yang sudah disiapkan sebelumnya. Jika pembuatan direktori berhasil, maka fungsi akan mengirim pesan ke klien bahwa database baru telah berhasil dibuat. Namun, jika pembuatan direktori gagal, fungsi akan mengirim pesan bahwa pembuatan database baru gagal.
- **Menyimpan Informasi Database Baru**:
Setelah pembuatan database baru berhasil, fungsi akan menyimpan informasi tentang database tersebut di dalam suatu struktur data. Nama database akan disimpan di dalam array `databases`, diindeks oleh `num_databases`. Selain itu, jumlah tabel dalam database tersebut diatur ke 0 karena belum ada tabel yang dibuat.
- **Penambahan Jumlah Database**:
Jumlah database (`num_databases`) akan ditambahkan setelah database baru berhasil dibuat dan informasinya disimpan.

![CREATE_DB](/uploads/c4636e2f3c0bc3c7bbec7a9ff8be0f70/CREATE_DB.png)

2. Fungsi createCSVfile
```c
int createCSVFile(const char *databaseName, const char *tableName, int numColumns, struct Column *columns, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", databaseName, tableName);

    FILE *csvFile = fopen(filename, "w");
    if (csvFile == NULL) {
        printf("Error creating file '%s'\n", filename);
        return 0;
    }

    for (int i = 0; i < numColumns; i++) {
        fprintf(csvFile, "%s;%s,", columns[i].name, columns[i].type);
    }

    fprintf(csvFile, "\n");
    fclose(csvFile);

    return 1;
}
```
Fungsi `createCSVFile` bertujuan untuk membuat file CSV untuk sebuah tabel dalam sebuah database. Fungsi ini menerima beberapa parameter: `databaseName` (nama database), `tableName` (nama tabel), `numColumns` (jumlah kolom), `columns` (struktur data untuk kolom-kolom), dan `clientSocket` (socket untuk komunikasi dengan klien).
- **Pembentukan Nama File CSV**:
Fungsi menggunakan `snprintf` untuk membentuk path lengkap menuju file CSV yang akan dibuat. Path ini terdiri dari direktori "databases", nama database, dan nama tabel dengan format "databases/nama_database/nama_tabel.csv".
- **Membuka File CSV untuk Ditulis**:
Fungsi menggunakan `fopen` untuk membuka file tersebut dalam mode "w" (write mode) yang berarti file akan dibuat atau jika sudah ada, file tersebut akan di-overwrite. Jika pembukaan file gagal, fungsi akan mencetak pesan kesalahan ke konsol (melalui `printf`) dan mengembalikan nilai 0 sebagai indikasi bahwa pembuatan file gagal.
- **Menulis Header Kolom**:
Selanjutnya, fungsi menulis header kolom ke dalam file CSV. Dalam loop `for`, untuk setiap kolom, fungsi menggunakan `fprintf` untuk menuliskan nama kolom dan tipe kolom ke dalam file, dipisahkan oleh tanda koma (',') dan tanda titik koma (';'). Setelah menulis semua kolom, fungsi mencetak newline (\n) untuk berpindah ke baris baru.
- **Menutup File CSV**:
Setelah selesai menulis header kolom, file CSV ditutup dengan menggunakan `fclose`.
- **Nilai Kembalian**:
Fungsi mengembalikan nilai 1 jika proses pembuatan file CSV berhasil. Jika terjadi kegagalan dalam pembuatan file, fungsi akan mengembalikan nilai 0.

3. CREATE TABLE
```c
void createTable(const char *databaseName, const char *tableName, int numColumns, struct Column *columns) {
    for (int i = 0; i < num_databases; i++) {
        if (strcmp(databases[i].name, databaseName) == 0) {
            if (databases[i].num_tables >= MAX_TABLES) {
                printf("Maximum tables reached for database '%s'\n", databaseName);
                return;
            }

            struct Table *table = &databases[i].tables[databases[i].num_tables];
            strcpy(table->name, tableName);
            table->numColumns = numColumns;
            for (int j = 0; j < numColumns; j++) {
                strcpy(table->columns[j].name, columns[j].name);
                strcpy(table->columns[j].type, columns[j].type);
            }

            databases[i].num_tables++;
            return;
        }
    }
}
```
Fungsi `createTable` memiliki tujuan untuk membuat tabel baru di dalam sebuah database. Fungsi ini menerima beberapa parameter: `databaseName` (nama database), `tableName` (nama tabel baru yang akan dibuat), `numColumns` (jumlah kolom), dan `columns` (struktur data untuk kolom-kolom).
- **Pencarian Database**:
Fungsi melakukan iterasi melalui daftar database yang ada (disimpan dalam array `databases`). Masing-masing database dibandingkan dengan nama database yang diberikan (`databaseName`) untuk menemukan database yang sesuai.
- **Penambahan Tabel pada Database yang Sesuai**:
Jika fungsi menemukan database yang cocok, itu melakukan serangkaian langkah untuk menambahkan tabel baru ke dalam database tersebut.
- **Peningkatan Jumlah Tabel dalam Database**:
Setelah berhasil menambahkan tabel ke dalam database, fungsi meningkatkan jumlah tabel (`num_tables`) dalam database yang bersangkutan.
Fungsi ini bekerja berdasarkan asumsi bahwa struktur data `databases` menyimpan informasi tentang database dan tabel-tabel yang ada di dalamnya. Fungsi ini juga mengasumsikan bahwa parameter yang diberikan sesuai dengan struktur data yang telah ditetapkan sebelumnya.

![CREATE_TABLE](/uploads/af2f134e3a3a537ca4b428ba0990865a/CREATE_TABLE.png) 

4. DROP DATABASE
```c
void dropDatabase(char *databaseName, int clientSocket) {
    char cmd[100], response[100];
    
    snprintf(cmd, sizeof(cmd), "rm -r databases/%s", databaseName);

    int result = system(cmd);

    if (result == 0) {
        snprintf(response, sizeof(response), "Database %s dropped", databaseName);
    } else {
        snprintf(response, sizeof(response), "Failed to drop database %s", databaseName);
    }

    send(clientSocket, response, strlen(response), 0);
}
```
Fungsi `dropDatabase` memiliki tujuan untuk menghapus seluruh database beserta isinya. Fungsi ini menerima dua parameter: `databaseName` (nama database yang akan dihapus) dan `clientSocket` (socket untuk mengirim respons ke klien).
- **Pembentukan Perintah untuk Hapus Database**:
Fungsi menggunakan `snprintf` untuk membuat perintah sistem yang akan menjalankan perintah `rm -r` untuk menghapus direktori database beserta isinya. Perintah ini dibentuk dengan format "rm -r databases/nama_database".
- **Eksekusi Perintah Sistem**:
Fungsi menggunakan fungsi `system` untuk menjalankan perintah sistem yang telah dibentuk sebelumnya. Hasil dari eksekusi perintah tersebut disimpan dalam variabel `result`.
- **Penanganan Hasil Eksekusi Perintah**:
    - Jika nilai result yang dikembalikan adalah 0, itu berarti perintah sistem untuk menghapus database berhasil dieksekusi tanpa masalah. Fungsi akan mengirimkan pesan respons ke klien bahwa database telah berhasil dihapus.
    - Jika nilai result tidak sama dengan 0, hal ini menunjukkan bahwa ada masalah dalam proses penghapusan database. Fungsi akan mengirim pesan respons ke klien bahwa penghapusan database gagal.
- **Pengiriman Respons ke Klien**:
Setelah mengeksekusi perintah dan menangani hasilnya, fungsi akan menggunakan `send` untuk mengirim respons ke klien menggunakan socket yang telah diberikan. Respons ini berisi pesan yang mengindikasikan apakah penghapusan database berhasil atau gagal.

![DROP_DB](/uploads/26ddf2f34c80883cb6241b0e98b27cb4/DROP_DB.png)
5. DROP TABLE
```c
void dropTable(char *tableName, int clientSocket) {
    char response[100], csvFile[100];

    if (strlen(selectedDB) == 0) {
        snprintf(response, sizeof(response), "Database %s not selected", selectedDB);
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    sprintf(csvFile, "databases/%s/%s.csv", selectedDB, tableName);

    int result = remove(csvFile);

    if (result == 0) {
        snprintf(response, sizeof(response), "Table %s dropped", tableName);
    } else {
        snprintf(response, sizeof(response), "Failed to drop table %s", tableName);
    }

    send(clientSocket, response, strlen(response), 0);
}
```
Fungsi `dropTable` merupakan sebuah fungsi yang bertujuan untuk menghapus tabel dari sebuah database. Fungsi ini memiliki dua parameter: `tableName`, yang merupakan nama tabel yang akan dihapus, dan `clientSocket`, yang digunakan untuk mengirim respons ke klien.
- **Pengecekan Database Terpilih**: 
Fungsi ini pertama-tama memeriksa apakah database sudah dipilih sebelumnya atau belum. Hal ini dilakukan dengan memeriksa panjang string dari variabel `selectedDB`. Jika panjangnya adalah 0, itu berarti tidak ada database yang dipilih sebelumnya. Fungsi akan mengirim pesan ke klien bahwa tidak ada database yang dipilih dan menghentikan proses.
- **Pembentukan Path File CSV Tabel**:
Fungsi menggunakan `sprintf` untuk membuat path lengkap dari file CSV tabel yang akan dihapus dengan format "databases/nama_database/nama_tabel.csv".
- **Menghapus File Tabel**:
Fungsi menggunakan `remove` untuk menghapus file tabel yang telah dibentuk sebelumnya. Jika penghapusan berhasil (nilai return `result` sama dengan 0), fungsi akan mengirim pesan ke klien bahwa tabel telah berhasil dihapus. Namun, jika penghapusan gagal, fungsi akan mengirim pesan bahwa gagal menghapus tabel.
- **Pengiriman Respons ke Klien**:
Setelah operasi penghapusan selesai, fungsi akan mengirimkan respons ke klien menggunakan socket yang telah diberikan. Respons ini berisi pesan yang mengindikasikan apakah tabel berhasil dihapus atau tidak.

![DROP_TABLE](/uploads/0779701ff062a43d9b711560b85b6e21/DROP_TABLE.png)

6. DROP COLUMN
```c
void dropColumn(const char *tableName, const char *columnName, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", selectedDB, tableName);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile == NULL) {
        printf("Error opening file '%s'\n", filename);
        const char *response = "Table or database not found";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    char tempFilename[500];
    snprintf(tempFilename, sizeof(tempFilename), "databases/%s/temp.csv", selectedDB);

    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Error creating temp file\n");
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        return;
    }

    char line[16384];
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        // Empty file
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    char *token;
    char *rest = line;
    int found = 0;

    // Check if the column exists
    while ((token = strtok_r(rest, ";", &rest)) != NULL) {
        if (strcmp(token, columnName) == 0) {
            found = 1;
            break;
        }
        fprintf(tempFile, "%s;", token);
    }

    if (!found) {
        // Column not found
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    fprintf(tempFile, "\n");

    while (fgets(line, sizeof(line), csvFile) != NULL) {
        rest = line;
        while ((token = strtok_r(rest, ";", &rest)) != NULL) {
            if (strcmp(token, columnName) != 0) {
                fprintf(tempFile, "%s;", token);
            }
        }
        fprintf(tempFile, "\n");
    }

    fclose(csvFile);
    fclose(tempFile);

    if (remove(filename) != 0) {
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (rename(tempFilename, filename) != 0) {
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    const char *response = "Column dropped successfully";
    send(clientSocket, response, strlen(response), 0);
}
```
Fungsi `dropColumn` di atas merupakan sebuah fungsi yang bertujuan untuk menghapus sebuah kolom dari sebuah file CSV yang merepresentasikan sebuah tabel dalam sebuah database sederhana.
- **Membuka File CSV Tabel**:
    - Fungsi pertama kali membangun path file CSV berdasarkan nama database dan nama tabel yang diberikan sebagai parameter.
    - Membuka file CSV untuk tabel yang ditentukan. Jika gagal membuka file tersebut, fungsi mengirim pesan error ke `clientSocket` dan mengembalikan kontrol.
- **Membuat File Sementara**:
    - Fungsi mencoba membuat file sementara untuk menyimpan hasil perubahan.
    - Jika gagal membuat file sementara, fungsi mengirim pesan error ke `clientSocket`, menutup file yang telah terbuka, dan mengembalikan kontrol.
- **Membaca Header Kolom**:
    - Fungsi membaca baris pertama dari file CSV yang berisi nama-nama kolom.
    - Jika file kosong, fungsi mengirim pesan bahwa kolom tidak ditemukan dan mengembalikan kontrol.
- **Mengecek Keberadaan Kolom**:
    - Fungsi kemudian memeriksa keberadaan kolom yang ingin dihapus dengan membaca baris pertama (nama kolom) dan mencari nama kolom yang sesuai.
    - Jika tidak ditemukan, fungsi mengirim pesan ke `clientSocket` bahwa kolom tidak ditemukan dan mengembalikan kontrol.
- **Menghapus Kolom**:
    - Fungsi menyalin isi file CSV ke file sementara, kecuali nilai yang terkait dengan kolom yang ingin dihapus.
    - Setelah semua nilai terkait dengan kolom dihapus dari file sementara, file asli dihapus.
    - File sementara diubah namanya menjadi nama file asli.
- **Pengiriman Pesan Status**:
Setelah selesai, fungsi mengirim pesan ke clientSocket yang memberi tahu apakah penghapusan kolom berhasil atau gagal.

### DML

1. INSERT

```c
void insertData(const char *databaseName, const char *tableName, int numValues, char **values, int clientSocket)
{
    char response[100];
    for (int i = 0; i < num_databases; i++)
    {
        if (strcmp(databases[i].name, databaseName) == 0)
        {
            for (int j = 0; j < databases[i].num_tables; j++)
            {
                if (strcmp(databases[i].tables[j].name, tableName) == 0)
                {
                    struct Table *table = &databases[i].tables[j];

                    char filename[500];
                    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", databaseName, tableName);
                    FILE *csvFile = fopen(filename, "a");
                    if (csvFile == NULL)
                    {
                        printf("Error opening file '%s'\n", filename);
                        return;
                    }

                    for (int k = 0; k < numValues; k++)
                    {
                        fprintf(csvFile, "%s,", values[k]);
                    }
                    fprintf(csvFile, "\n");

                    fclose(csvFile);

                    printf("Received INSERT INTO command.\n");
                    printf("Database: %s\n", databaseName);
                    printf("Table: %s\n", tableName);
                    printf("Values: ");
                    for (int i = 0; i < numValues; i++)
                    {
                        printf("%s ", values[i]);
                    }
                    printf("\n");

                    snprintf(response, sizeof(response), "Data inserted into table %s in database %s\n", tableName, databaseName);
                    send(clientSocket, response, strlen(response), 0);
                    return;
                }
            }
            snprintf(response, sizeof(response), "Table %s not found in database %s\n", tableName, databaseName);
            send(clientSocket, response, strlen(response), 0);
            return;
        }
    }
    snprintf(response, sizeof(response), "Database %s not found\n", databaseName);
    send(clientSocket, response, strlen(response), 0);
}
```

Fungsi ini melakukan pencarian database dan tabel yang sesuai berdasarkan nama yang diberikan. Setelah menemukan tabel yang sesuai, fungsi membuka file CSV terkait dalam mode "append" dan menulis nilai-nilai yang diberikan ke dalam file tersebut sebagai baris baru. Fungsi ini juga memberikan tanggapan ke klien mengenai keberhasilan atau kegagalan operasi.

2. UPDATE
``` c
void updateData(const char *tableName, const char *columnName, const char *value, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", selectedDB, tableName);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile == NULL) {
        printf("Error opening file '%s'\n", filename);
        const char *response = "Table or database not found";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    char tempFilename[500];
    snprintf(tempFilename, sizeof(tempFilename), "databases/%s/temp.csv", selectedDB);

    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Error creating temp file\n");
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        return;
    }

    char line[16384];
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        // Empty file
        const char *response = "No data to update";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    char *token;
    char *rest = line;
    int foundColumn = 0;
    int columnIndex = 0;

    // Check if the column exists
    while ((token = strtok_r(rest, ";", &rest)) != NULL) {
        if (strcmp(token, columnName) == 0) {
            foundColumn = 1;
            break;
        }
        fprintf(tempFile, "%s;", token);
        columnIndex++;
    }

    if (!foundColumn) {
        // Column not found
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    fprintf(tempFile, "\n");

    while (fgets(line, sizeof(line), csvFile) != NULL) {
        rest = line;
        columnIndex = 0;

        while ((token = strtok_r(rest, ";", &rest)) != NULL) {
            if (columnIndex == 0) {
                fprintf(tempFile, "%s;", token);
            } else if (columnIndex == foundColumn) {
                fprintf(tempFile, "%s;", value);
            } else {
                fprintf(tempFile, "%s;", token);
            }
            columnIndex++;
        }

        fprintf(tempFile, "\n");
    }

    fclose(csvFile);
    fclose(tempFile);

    if (remove(filename) != 0) {
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (rename(tempFilename, filename) != 0) {
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    const char *response = "Data updated successfully";
    send(clientSocket, response, strlen(response), 0);
}
```
Fungsi `updateData` bertujuan untuk memperbarui nilai data dalam suatu tabel pada database. 

- **Pembentukan Nama File**: Fungsi menggunakan snprintf untuk membentuk nama file tabel dengan format "databases/selectedDB/tableName.csv" dan menyimpannya dalam variabel filename.
- **Membuka File CSV**: Fungsi mencoba membuka file CSV dengan nama yang telah dibentuk. Jika file tidak dapat dibuka, fungsi mengirim pesan kesalahan ke klien dan keluar dari fungsi.
- **Membuat File Temporari**: Fungsi membuat file sementara ("temp.csv") untuk menyimpan perubahan yang akan dilakukan. Jika file sementara tidak dapat dibuat, fungsi mengirim pesan kesalahan ke klien, menutup file CSV, dan keluar dari fungsi.
- **Penanganan File CSV Kosong**: Jika file CSV kosong, fungsi mengirim pesan ke klien bahwa tidak ada data yang dapat diperbarui, menutup kedua file, dan keluar dari fungsi.
- **Pengecekan dan Pencarian Kolom**: Fungsi membaca baris pertama dari file CSV untuk menemukan kolom yang sesuai dengan parameter columnName. Jika kolom ditemukan, fungsi menyimpan indeks kolom tersebut dan melanjutkan proses.
- **Memproses Setiap Baris**: Fungsi membaca setiap baris dari file CSV dan memproses nilai kolom sesuai dengan indeks yang ditemukan. Nilai yang diperbarui disimpan dalam file sementara.
- **Penggantian File CSV**: Setelah semua baris diproses, file CSV asli dihapus, dan file sementara diubah namanya menjadi file CSV asli. Jika operasi ini gagal, fungsi mengirim pesan kesalahan ke klien.

### Logging
``` c
void createLog(const char *username, const char *command)
{
    struct stat st = {0};

    if (stat("src", &st) == -1) {
        mkdir("src", 0777);
    }

    FILE *logFile = fopen("src/log.txt", "a");
    if (logFile == NULL) {
        printf("Failed to open log file.\n");
        return;
    }

    chmod("src/log.txt", S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

    time_t now;
    time(&now);
    struct tm *localTime = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localTime);

    if (getuid() == 0) {
        fprintf(logFile, "%s:root:%s\n", timestamp, command);
    } else {
        fprintf(logFile, "%s:%s:%s\n", timestamp, username, command);
    }

    fclose(logFile);
}
```
Fungsi `createLog` bertujuan untuk membuat dan memperbarui file log (log.txt) dengan mencatat aktivitas pengguna.

- **Pemeriksaan Direktori "src"**: Fungsi dimulai dengan melakukan pemeriksaan apakah direktori "src" sudah ada atau belum. Jika belum ada, fungsi membuat direktori tersebut menggunakan fungsi `mkdir`.
- **Membuka File Log**: Selanjutnya, fungsi mencoba membuka file log "src/log.txt" dengan mode "a" (append) untuk menambahkan log baru. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan dan keluar dari fungsi.
- **Mengatur Izin File Log**: Setelah membuka file log, fungsi menggunakan chmod untuk mengatur izin file log dengan hak akses yang lebih fleksibel (read-write untuk pemilik, grup, dan lainnya).
- **Perekaman Waktu**: Fungsi mendapatkan waktu sistem saat ini menggunakan time dan mengonversinya menjadi waktu lokal dengan bantuan localtime. Timestamp (stempel waktu) dibuat menggunakan strftime dengan format yang ditentukan.
- **Penulisan Log**: Fungsi menuliskan log ke file dengan menggunakan fprintf. Jika pengguna memiliki privilege root, log mencatat aktivitas dengan label "root"; jika tidak, log mencatat aktivitas dengan menyertakan nama pengguna.

Fungsi `createLog` tersebut dipanggil setelah user memasukkan command, tepatnya ada pada kode sebagai berikut:
``` c
while (1) {
        printf("Enter your command: ");
        fgets(command, sizeof(command), stdin);

        size_t command_len = strlen(command);
        if (write(sock, command, command_len) != command_len) {
            perror("Failed to send message to server");
            exit(EXIT_FAILURE);
        }

        createLog(username, command);

        // ...
```

![LOG](/uploads/f620077d570a07586cf842816241f3f3/LOG.png)