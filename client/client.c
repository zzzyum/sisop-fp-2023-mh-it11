#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define PORT 8080

void createLog(const char *username, const char *command)
{
    struct stat st = {0};

    if (stat("src", &st) == -1) {
        mkdir("src", 0777);
    }

    FILE *logFile = fopen("src/log.txt", "a");
    if (logFile == NULL) {
        printf("Failed to open log file.\n");
        return;
    }

    chmod("src/log.txt", S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

    time_t now;
    time(&now);
    struct tm *localTime = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", localTime);

    if (getuid() == 0) {
        fprintf(logFile, "%s:root:%s\n", timestamp, command);
    } else {
        fprintf(logFile, "%s:%s:%s\n", timestamp, username, command);
    }

    fclose(logFile);
}

int checkUser(const char *username, const char *password)
{
    FILE *fp = fopen("src/users.txt", "r");

    if (fp == NULL)
    {
        printf("Failed to open users file.\n");
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), fp))
    {
        char user[100], pass[100];
        int matched = sscanf(line, "%99[^,],%99[^,\n]", user, pass);

        if (matched >= 2 && strcmp(user, username) == 0 && strcmp(pass, password) == 0)
        {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}

int authorizeUser(const char *username, const char *database)
{
    if (getuid() == 0)
    {
        return 1;
    }

    FILE *fp = fopen("src/permissions.txt", "r");
    if (fp == NULL)
    {
        printf("Failed to open permissions file.\n");
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), fp))
    {
        char db[100], user[100];
        int matched = sscanf(line, "%99[^,],%99[^,\n]", db, user);

        if (matched >= 2 && strcmp(db, database) == 0 && strcmp(user, username) == 0)
        {
            fclose(fp);
            return 1;
        }
    }

    fclose(fp);
    return 0;
}

void grantPermission(const char *database, const char *username)
{
    if (getuid() != 0)
    {
        printf("Only root can grant permissions.\n");
        return;
    }

    FILE *fp = fopen("src/permissions.txt", "a");
    if (fp == NULL)
    {
        printf("Failed to open permissions file.\n");
        return;
    }

    fprintf(fp, "%s,%s\n", database, username);
    fclose(fp);

    printf("Permission granted successfully.\n");
}

int main(int argc, char *argv[]) {
    int sock = 0;
    char buffer[16384] = {0};
    char username[128], password[128], command[16384];
    struct sockaddr_in serverAddress;

    if (getuid() != 0)
    {
        int opt;
        while ((opt = getopt(argc, argv, "u:p:")) != -1)
        {
            switch (opt)
            {
            case 'u':
                strncpy(username, optarg, sizeof(username));
                break;
            case 'p':
                strncpy(password, optarg, sizeof(password));
                break;
            default:
                fprintf(stderr, "Usage: %s -u username -p password\n", argv[0]);
                return EXIT_FAILURE;
            }
        }

        if (username[0] == '\0' || password[0] == '\0')
        {
            fprintf(stderr, "Both username and password are required.\n");
            return EXIT_FAILURE;
        }

        if (!checkUser(username, password))
        {
            printf("Invalid username or password.\n");
            return EXIT_FAILURE;
        }
    }

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("There was an error when creating the socket.");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr) <= 0) {
        perror("Incorrect / unsupported IP address.");
        exit(EXIT_FAILURE);
    }

    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Failed to connect to server.");
        exit(EXIT_FAILURE);
    }

    while (1) {
        printf("Enter your command: ");
        fgets(command, sizeof(command), stdin);

        size_t command_len = strlen(command);
        if (write(sock, command, command_len) != command_len) {
            perror("Failed to send message to server");
            exit(EXIT_FAILURE);
        }

        createLog(username, command);

        if (getuid() == 0)
        {
            if (strncmp(command, "CREATE USER", 11) == 0)
            {
                char newuser[100], newpass[100];
                sscanf(command, "CREATE USER %s IDENTIFIED BY %s", newuser, newpass);

                FILE *fp = fopen("src/users.txt", "a");
                if (fp == NULL)
                {
                    printf("Failed to open users file.\n");
                    return -1;
                }

                fprintf(fp, "%s,%s\n", newuser, newpass);
                fclose(fp);

                printf("User created successfully.\n");

                continue;
            }
            else if (strncmp(command, "GRANT PERMISSION", 16) == 0)
            {
                char db[100], grantUser[100];
                sscanf(command, "GRANT PERMISSION %s INTO %s", db, grantUser);
                grantPermission(db, grantUser);

                continue;
            } else {
                read(sock, buffer, 1024);
                printf("%s\n", buffer);
            }
        } else {
            if (strncmp(command, "USE", 3) == 0)
            {
                char database[100];
                sscanf(command, "USE %s", database);

                if (!authorizeUser(username, database))
                {
                    printf("You don't have permission to access this database.\n");
                    continue;
                }
            }

            read(sock, buffer, 1024);
            printf("%s\n", buffer);
        }
    }

    close(sock);
    return 0;
}
