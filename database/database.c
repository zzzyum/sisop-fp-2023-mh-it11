#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <syslog.h>

#define PORT 8080

#define MAX_DATABASES 10 
#define MAX_TABLES 10    
#define MAX_COLUMNS 10

struct Column {
    char name[50];
    char type[20];
};

struct Table {
    char name[50];
    int numColumns;
    struct Column columns[MAX_COLUMNS];
};

struct Database {
    char name[50];
    int num_tables;
    struct Table tables[MAX_TABLES];
};

struct Database databases[MAX_DATABASES];
int num_databases = 0;
char selectedDB[50];

void createDatabase(const char *databaseName, int clientSocket) {
    struct stat st = {0};
    if (stat("databases", &st) == -1) {
        mkdir("databases", 0777);
    }

    char databasePath[100];
    snprintf(databasePath, sizeof(databasePath), "databases/%s", databaseName);

    if (num_databases >= MAX_DATABASES) {
        const char *response = "Maximum databases reached";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (mkdir(databasePath, 0777) == -1) {
        const char *response = "Failed to create database!";
        send(clientSocket, response, strlen(response), 0);
        return;
    } else {
        char response[100];
        snprintf(response, sizeof(response), "Database '%s' created!", databaseName);
        send(clientSocket, response, strlen(response), 0);
    }

    strcpy(databases[num_databases].name, databaseName);
    databases[num_databases].num_tables = 0;
    num_databases++;
}

int createCSVFile(const char *databaseName, const char *tableName, int numColumns, struct Column *columns, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", databaseName, tableName);

    FILE *csvFile = fopen(filename, "w");
    if (csvFile == NULL) {
        printf("Error creating file '%s'\n", filename);
        return 0;
    }

    for (int i = 0; i < numColumns; i++) {
        fprintf(csvFile, "%s;%s,", columns[i].name, columns[i].type);
    }

    fprintf(csvFile, "\n");
    fclose(csvFile);

    return 1;
}

void createTable(const char *databaseName, const char *tableName, int numColumns, struct Column *columns) {
    for (int i = 0; i < num_databases; i++) {
        if (strcmp(databases[i].name, databaseName) == 0) {
            if (databases[i].num_tables >= MAX_TABLES) {
                printf("Maximum tables reached for database '%s'\n", databaseName);
                return;
            }

            struct Table *table = &databases[i].tables[databases[i].num_tables];
            strcpy(table->name, tableName);
            table->numColumns = numColumns;
            for (int j = 0; j < numColumns; j++) {
                strcpy(table->columns[j].name, columns[j].name);
                strcpy(table->columns[j].type, columns[j].type);
            }

            databases[i].num_tables++;
            return;
        }
    }
}

void dropDatabase(char *databaseName, int clientSocket) {
    char cmd[100], response[100];
    
    snprintf(cmd, sizeof(cmd), "rm -r databases/%s", databaseName);

    int result = system(cmd);

    if (result == 0) {
        snprintf(response, sizeof(response), "Database %s dropped", databaseName);
    } else {
        snprintf(response, sizeof(response), "Failed to drop database %s", databaseName);
    }

    send(clientSocket, response, strlen(response), 0);
}

void dropTable(char *tableName, int clientSocket) {
    char response[100], csvFile[100];

    if (strlen(selectedDB) == 0) {
        snprintf(response, sizeof(response), "Database %s not selected", selectedDB);
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    sprintf(csvFile, "databases/%s/%s.csv", selectedDB, tableName);

    int result = remove(csvFile);

    if (result == 0) {
        snprintf(response, sizeof(response), "Table '%s' dropped", tableName);
    } else {
        snprintf(response, sizeof(response), "Failed to drop table %s", tableName);
    }

    send(clientSocket, response, strlen(response), 0);
}

void dropColumn(const char *tableName, const char *columnName, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", selectedDB, tableName);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile == NULL) {
        printf("Error opening file '%s'\n", filename);
        const char *response = "Table or database not found";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    char tempFilename[500];
    snprintf(tempFilename, sizeof(tempFilename), "databases/%s/temp.csv", selectedDB);

    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Error creating temp file\n");
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        return;
    }

    char line[16384];
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        // Empty file
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    char *token;
    char *rest = line;
    int found = 0;

    // Check if the column exists
    while ((token = strtok_r(rest, ";", &rest)) != NULL) {
        if (strcmp(token, columnName) == 0) {
            found = 1;
            break;
        }
        fprintf(tempFile, "%s;", token);
    }

    if (!found) {
        // Column not found
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    fprintf(tempFile, "\n");

    while (fgets(line, sizeof(line), csvFile) != NULL) {
        rest = line;
        while ((token = strtok_r(rest, ";", &rest)) != NULL) {
            if (strcmp(token, columnName) != 0) {
                fprintf(tempFile, "%s;", token);
            }
        }
        fprintf(tempFile, "\n");
    }

    fclose(csvFile);
    fclose(tempFile);

    if (remove(filename) != 0) {
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (rename(tempFilename, filename) != 0) {
        const char *response = "Failed to drop column";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    const char *response = "Column dropped successfully";
    send(clientSocket, response, strlen(response), 0);
}

void insertData(const char *databaseName, const char *tableName, int numValues, char **values, int clientSocket)
{
    char response[100];
    for (int i = 0; i < num_databases; i++)
    {
        if (strcmp(databases[i].name, databaseName) == 0)
        {
            for (int j = 0; j < databases[i].num_tables; j++)
            {
                if (strcmp(databases[i].tables[j].name, tableName) == 0)
                {
                    struct Table *table = &databases[i].tables[j];

                    char filename[500];
                    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", databaseName, tableName);
                    FILE *csvFile = fopen(filename, "a");
                    if (csvFile == NULL)
                    {
                        printf("Error opening file '%s'\n", filename);
                        return;
                    }

                    for (int k = 0; k < numValues; k++)
                    {
                        fprintf(csvFile, "%s,", values[k]);
                    }
                    fprintf(csvFile, "\n");

                    fclose(csvFile);

                    printf("Received INSERT INTO command.\n");
                    printf("Database: %s\n", databaseName);
                    printf("Table: %s\n", tableName);
                    printf("Values: ");
                    for (int i = 0; i < numValues; i++)
                    {
                        printf("%s ", values[i]);
                    }
                    printf("\n");

                    snprintf(response, sizeof(response), "Data inserted into table %s in database %s\n", tableName, databaseName);
                    send(clientSocket, response, strlen(response), 0);
                    return;
                }
            }
            snprintf(response, sizeof(response), "Table %s not found in database %s\n", tableName, databaseName);
            send(clientSocket, response, strlen(response), 0);
            return;
        }
    }
    snprintf(response, sizeof(response), "Database %s not found\n", databaseName);
    send(clientSocket, response, strlen(response), 0);
}

void updateData(const char *tableName, const char *columnName, const char *value, int clientSocket) {
    char filename[500];
    snprintf(filename, sizeof(filename), "databases/%s/%s.csv", selectedDB, tableName);

    FILE *csvFile = fopen(filename, "r");
    if (csvFile == NULL) {
        printf("Error opening file '%s'\n", filename);
        const char *response = "Table or database not found";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    char tempFilename[500];
    snprintf(tempFilename, sizeof(tempFilename), "databases/%s/temp.csv", selectedDB);

    FILE *tempFile = fopen(tempFilename, "w");
    if (tempFile == NULL) {
        printf("Error creating temp file\n");
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        return;
    }

    char line[16384];
    if (fgets(line, sizeof(line), csvFile) == NULL) {
        // Empty file
        const char *response = "No data to update";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    char *token;
    char *rest = line;
    int foundColumn = 0;
    int columnIndex = 0;

    // Check if the column exists
    while ((token = strtok_r(rest, ";", &rest)) != NULL) {
        if (strcmp(token, columnName) == 0) {
            foundColumn = 1;
            break;
        }
        fprintf(tempFile, "%s;", token);
        columnIndex++;
    }

    if (!foundColumn) {
        // Column not found
        const char *response = "Column not found";
        send(clientSocket, response, strlen(response), 0);
        fclose(csvFile);
        fclose(tempFile);
        return;
    }

    fprintf(tempFile, "\n");

    while (fgets(line, sizeof(line), csvFile) != NULL) {
        rest = line;
        columnIndex = 0;

        while ((token = strtok_r(rest, ";", &rest)) != NULL) {
            if (columnIndex == 0) {
                fprintf(tempFile, "%s;", token);
            } else if (columnIndex == foundColumn) {
                fprintf(tempFile, "%s;", value);
            } else {
                fprintf(tempFile, "%s;", token);
            }
            columnIndex++;
        }

        fprintf(tempFile, "\n");
    }

    fclose(csvFile);
    fclose(tempFile);

    if (remove(filename) != 0) {
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    if (rename(tempFilename, filename) != 0) {
        const char *response = "Failed to update data";
        send(clientSocket, response, strlen(response), 0);
        return;
    }

    const char *response = "Data updated successfully";
    send(clientSocket, response, strlen(response), 0);
}

void *clientHandler(void *socketDescription) {
    int *client = (int *)socketDescription;
    char command[16384];

    while (1) {
        memset(command, 0, sizeof(command));
        recv(*client, command, sizeof(command), 0);

        printf("%s", command);

        if (strncmp(command, "USE", 3) == 0)
        {
            char databaseName[50];
            sscanf(command, "USE %s", databaseName);
            strcpy(selectedDB, databaseName);
            
            char response[100];
            snprintf(response, sizeof(response), "User logged to %s", selectedDB);
            send(*client, response, strlen(response), 0);

            continue;
        } else if (strncmp(command, "CREATE DATABASE", 15) == 0) {
            char databaseName[50];
            sscanf(command, "CREATE DATABASE %s", databaseName);
            strcpy(selectedDB, databaseName);

            createDatabase(selectedDB, *client);

            continue;
        } else if (strncmp(command, "CREATE TABLE", 12) == 0) {
            char tableName[50];
            int numColumns = 0;

            struct Column columns[MAX_COLUMNS];

            char *token;
            char *rest = command + 13;

            token = strtok_r(rest, " ", &rest);
            strcpy(selectedDB, token);
            token = strtok_r(rest, " ", &rest);
            strcpy(tableName, token);

            while ((token = strtok_r(rest, ",", &rest)) != NULL && numColumns < MAX_COLUMNS) {
                char col_name[50], col_type[20];
                sscanf(token, "%s %s", col_name, col_type);

                strcpy(columns[numColumns].name, col_name);
                strcpy(columns[numColumns].type, col_type);
                numColumns++;
            }

            createTable(selectedDB, tableName, numColumns, columns);
            if (createCSVFile(selectedDB, tableName, numColumns, columns, *client)) {
                const char *response = "Table created";
                send(*client, response, strlen(response), 0);
            } else {
                const char *response = "Failed to create table or CSV file";
                send(*client, response, strlen(response), 0);
            }

            continue;
        } else if (strncmp(command, "DROP DATABASE", 13) == 0) {
            char databaseName[50], path[50];
            sscanf(command, "DROP DATABASE %s", databaseName);

            dropDatabase(databaseName, *client);

            continue;
        } else if (strncmp(command, "DROP TABLE", 10) == 0) {
            char tableName[50];
            sscanf(command, "DROP TABLE %s", tableName);

            dropTable(tableName, *client);
            continue;
        } else if (strncmp(command, "DROP COLUMN", 12) == 0) {
            char columnName[50], tableName[50];
            sscanf(command, "DROP COLUMN %s FROM %s", columnName, tableName);

            dropColumn(tableName, columnName, *client);

            continue;
        } else if (strncmp(command, "INSERT INTO", 11) == 0) {
            char tableName[50];
            int numValues = 0;
            char *values[MAX_COLUMNS];

            char *token;
            char *rest = command + 12;

            token = strtok_r(rest, " ", &rest);
            strcpy(selectedDB, token);
            token = strtok_r(rest, " ", &rest);
            strcpy(tableName, token);

            while ((token = strtok_r(rest, " ", &rest)) != NULL && numValues < MAX_COLUMNS)
            {
                values[numValues] = strdup(token);
                numValues++;
            }

            insertData(selectedDB, tableName, numValues, values, *client);

            for (int i = 0; i < numValues; i++)
            {
                free(values[i]);
            }

            continue;
        } else if (strncmp(command, "UPDATE", 6) == 0) {
            char tableName[50], columnName[50], value[50];
            sscanf(command, "UPDATE %s SET %[^=]='%[^']'", tableName, columnName, value);

            updateData(tableName, columnName, value, *client);
            continue;
        } else {

        }
    }

    close(*client);
    free(client);
    pthread_exit(NULL);
}

int main() {
    int opt = 1;
    int server_fd, newSocket, c;
    struct sockaddr_in serverAddress, clientAddress;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Error creating socket.");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        perror("Bind failed.");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 1) < 0) {
        perror("Listen failed.");
        exit(EXIT_FAILURE);
    }

    c = sizeof(struct sockaddr_in);

    if ((newSocket = accept(server_fd, (struct sockaddr *)&clientAddress, (socklen_t *)&c)) < 0) {
        perror("Failed to connect server with client.");
        return 1;
    }

    pthread_t client_thread;

    int *clientSocket = malloc(sizeof(int));
    *clientSocket = newSocket;

    if (pthread_create(&client_thread, NULL, clientHandler, (void *)clientSocket) < 0) {
        perror("Unable to create thread");
        return 1;
    }

    pthread_join(client_thread, NULL);

    close(server_fd);
    return 0;
}